defmodule Products.ShopsTest do
  use Products.DataCase

  alias Products.Shops

  describe "product_from_list ::" do
    alias Products.Shop.Product

    @valid_list ["Eggroll","http://dummyimage.com/103x112.bmp/cc0000/ffffff","99689","94899","0d9803c2-d16d-4f38-826c-f9869f619166","Weber, Cremin and Hermiston"]
    @update_list ["Update","http://dummyimage.com/103x112.bmp/cc0000/ffffff","99689","94899","0d9803c2-d16d-4f38-826c-f9869f619166","Weber, Cremin and Hermiston"]
    @invalid_list ["Eggroll","http://dummyimage.com/103x112.bmp/cc0000/ffffff","99689","94899","0d9803c2-d16d-4f38-826c-f9869f619166"]

    test "with valid list returns new product" do
      assert {:ok, product} = Shops.product_from_list(@valid_list)
    end

    test "returns error for invalid list" do
      assert {:error, {:invalid, _}} = Shops.product_from_list(@invalid_list)
    end

    test "successfully updates fot new data in row" do
      assert {:ok, product} = Shops.product_from_list(@update_list)
      assert "Update" == product.name
    end
  end

  describe "list_products ::" do
    setup do
      Application.put_env(:products, :data_file, "VALID_DATA.csv")
      Products.Retreiver.get_data()
      :ok
    end

    test "list products without where clauses" do
      assert 4 = Shops.list_products([]) |> Enum.count()
    end

    test "list only products for single wendor" do
      assert 1 = Shops.list_products([producer: "McLaughlin-Bosco"]) |> Enum.count()
    end

    test "Pagination works as expected" do
      assert %{page_number: 2, page_size: 3, entries: [_]} = Shops.list_products([], [page: 2, page_size: 3])
    end
  end
end
