defmodule Products.RetrieverTest do
  use Products.DataCase

  describe "Reading input CSV" do
    test "CSV is valid" do
      Application.put_env(:products, :data_file, "VALID_DATA.csv")

      Products.Retreiver.get_data()

      assert 4 == Repo.all(Products.Shop.Product) |> Enum.count()
    end

    test "CSV is invalid" do
      Application.put_env(:products, :data_file, "INVALID_DATA.csv")

      Products.Retreiver.get_data()

      assert 3 == Repo.all(Products.Shop.Product) |> Enum.count()
    end
  end
end
