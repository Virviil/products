defmodule ProductsWeb.ProductControllerTest do
  use ProductsWeb.ConnCase

  alias Products.Shops

  describe "index" do
    test "lists all products", %{conn: conn} do
      conn = get(conn, Routes.product_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Products"
    end
  end
end
