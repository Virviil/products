defmodule Products.Retreiver do
  require Logger

  alias Products.Shops
  @default_file "MOCK_DATA.csv"

  defp file_path do
    :code.priv_dir(:products)
    |> Path.join(Application.get_env(:products, :data_file, @default_file))
  end

  def get_data do
    file_path()
    |> File.stream!()
    |> CSV.decode()
    |> Stream.drop(1)
    |> Flow.from_enumerable(max_demand: 50)
    |> Flow.filter(fn
      {:ok, _data} ->
        true

      {:error, reason} ->
        Logger.error(reason)
        false
    end)
    |> Flow.map(fn {:ok, data_row} -> data_row end)
    |> Flow.map(&Shops.product_from_list/1)
    |> Flow.run()
  end
end
