defmodule Products.Shops do
  @moduledoc """
  The Shops context.
  """

  import Ecto.Query, warn: false
  alias Products.Repo

  alias Products.Shop.Product

  def list_products(clauses, options \\ []) do
    Product
    |> where(^clauses)
    |> Repo.paginate(options)
  end

  def product_from_list([name, photo_url, barcode, price, uid, producer]) do
    case Repo.get(Product, uid) do
      nil -> %Product{id: uid}
      product -> product
    end
    |> Product.changeset(%{
      name: name,
      photo_url: photo_url,
      barcode: barcode,
      price: price,
      producer: producer
    })
    |> Repo.insert_or_update()
  end
  def product_from_list(invalid_list) do
    {:error, {:invalid, invalid_list}}
  end
end
