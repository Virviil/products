defmodule Products.Shop.Product do
  use Ecto.Schema
  import Ecto.Changeset

  @cast_fields [:id, :name, :photo_url, :barcode, :price, :producer]

  @primary_key {:id, :binary_id, autogenerate: true}
  @derive {Phoenix.Param, key: :id}
  schema "products" do
    field :barcode, :string
    field :name, :string
    field :photo_url, :string
    field :price, :integer
    field :producer, :string

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, @cast_fields)
    |> validate_required([:id, :name, :price])
  end

  defimpl Jason.Encoder do
    @value_fields [:id, :name, :photo_url, :barcode, :price, :producer]
    def encode(product, opts) do
      Jason.Encode.map(Map.take(product, @value_fields), opts)
    end
  end
end
