defmodule ProductsWeb.ProductView do
  use ProductsWeb, :view

  def render("index.json", %{page: page}) do Map.take(page, [:entries, :page_number, :page_size, :total_pages])
  end
end
