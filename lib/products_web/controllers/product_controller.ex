defmodule ProductsWeb.ProductController do
  use ProductsWeb, :controller

  alias Products.Shops

  def index(conn, params) do
    render(conn, :index,
      page:
        Shops.list_products(
          case Map.get(params, "producer", nil) do
            nil -> []
            producer -> [producer: producer]
          end,
          page: Map.get(params, "page", 1),
          page_size: Map.get(params, "page_size", 25)
        )
    )
  end
end
