defmodule ProductsWeb.Router do
  use ProductsWeb, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ProductsWeb do
    pipe_through :browser

    get "/products", ProductController, :index
  end
end
