# Products

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && yarn`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.



## Input data

By default, data is red from `priv/MOCK_DATA.csv`. The name of the file can be changed in configuration, under path `:products / :data_file` config path.
This file can be separately specified for `dev` and `test` environments.

Data updating is performed once a day at midnight.

All invalid lines in CSV are skipped, all valid lines are still available for working.

## Content-Type

The same endpoint can be used both for `HTML` and `Json` requests.

In order to perform `Json` request:

* `Accept` header with `application/json` value should be specified for a request.

In order to perform `HTML` request:

* Simply call requred route in the browser.


## Available routes

`/products` - list of all products. Can be called with get params:

* `producer` - name of a producer to show product only with given producer
* `page` - number of a page to paginate
* `page_size` - size of a page to paginate
