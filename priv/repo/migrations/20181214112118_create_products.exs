defmodule Products.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :photo_url, :string
      add :barcode, :string
      add :price, :integer
      add :producer, :string

      timestamps()
    end

  end
end
