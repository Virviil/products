# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :products,
  ecto_repos: [Products.Repo]

config :products,
  data_file: "MOCK_DATA.csv"

# Configures the endpoint
config :products, ProductsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rEq1KCkmoOLU3VRwaXSpyFDEOQLwdk/x+WYB9N2dj4+EQbKxawBpa9jTRUHt6xBu",
  render_errors: [view: ProductsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Products.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :products, Products.Scheduler,
  jobs: [
    {"@daily", {Products.Retreiver, :update_data, []}}
  ]
